dEQP-VK.api.driver_properties.conformance_version,Fail

# DXIL does not have a way to express these yet. Hopefully incoming with shader model 6.8
# See https://github.com/microsoft/hlsl-specs/issues/30
dEQP-VK.glsl.texture_functions.texture.sampler1darrayshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.texture.sampler1dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.texture.sampler2dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.texture.samplercubeshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler1darrayshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler1darrayshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler1dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler1dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler2darrayshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler2darrayshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler2dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.sampler2dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegrad.samplercubeshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegrad.samplercubeshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler1darrayshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler1darrayshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler1dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler1dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler2darrayshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler2darrayshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler2dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.texturegradoffset.sampler2dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.textureoffset.sampler1darrayshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureoffset.sampler1dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureoffset.sampler2dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureproj.sampler1dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureproj.sampler2dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojgrad.sampler1dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojgrad.sampler1dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.textureprojgrad.sampler2dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojgrad.sampler2dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.textureprojgradoffset.sampler1dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojgradoffset.sampler1dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.textureprojgradoffset.sampler2dshadow_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojgradoffset.sampler2dshadow_vertex,Fail
dEQP-VK.glsl.texture_functions.textureprojoffset.sampler1dshadow_bias_fragment,Fail
dEQP-VK.glsl.texture_functions.textureprojoffset.sampler2dshadow_bias_fragment,Fail

# WARP/DXC bug, some phi/control flow lowering caused incorrect values to be loaded after the loop.
# Fix incoming: https://github.com/microsoft/DirectXShaderCompiler/pull/5180
dEQP-VK.glsl.loops.generic.do_while_dynamic_iterations.basic_highp_float_fragment,Fail
dEQP-VK.glsl.loops.generic.do_while_dynamic_iterations.basic_highp_float_vertex,Fail
dEQP-VK.glsl.loops.generic.do_while_dynamic_iterations.basic_mediump_float_fragment,Fail
dEQP-VK.glsl.loops.generic.do_while_dynamic_iterations.basic_mediump_float_vertex,Fail
dEQP-VK.glsl.loops.generic.do_while_uniform_iterations.basic_highp_float_fragment,Fail
dEQP-VK.glsl.loops.generic.do_while_uniform_iterations.basic_highp_float_vertex,Fail
dEQP-VK.glsl.loops.generic.do_while_uniform_iterations.basic_mediump_float_fragment,Fail
dEQP-VK.glsl.loops.generic.do_while_uniform_iterations.basic_mediump_float_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.dowhile_trap_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.dowhile_trap_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.nested_sequence_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.nested_sequence_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.sequence_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.sequence_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.vector_counter_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_dynamic_iterations.vector_counter_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.dowhile_trap_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.dowhile_trap_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.nested_sequence_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.nested_sequence_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.sequence_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.sequence_vertex,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.vector_counter_fragment,Fail
dEQP-VK.glsl.loops.special.do_while_uniform_iterations.vector_counter_vertex,Fail
